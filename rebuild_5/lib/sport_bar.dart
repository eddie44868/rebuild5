import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:percent_indicator/percent_indicator.dart';

// ignore: must_be_immutable
class SportBar extends StatelessWidget {
  
  String imageUrl;
  String sport;
  String time;
  String total;
  double percent;
  double ukuran;

  SportBar({Key? key, required this.imageUrl, required this.sport, required this.time, required this.percent, required this.total, required this.ukuran, }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
      child: Row(
        children: [
          Container(
            height: 60,
            width: 60,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10), color: Colors.white),
            child: Image.asset(
              imageUrl,
              height: 40,
              width: 40,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              left: 20,
            ),
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      sport,
                      style: GoogleFonts.montserrat(
                          fontSize: 14, fontWeight: FontWeight.w500),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: ukuran),
                      child: Row(
                        children: [
                          Text(
                            time,
                            style: GoogleFonts.montserrat(
                                fontSize: 12,
                                fontWeight: FontWeight.w500,
                                color: Colors.grey),
                          ),
                          const SizedBox(
                            width: 20,
                          ),
                          Text(
                            total,
                            style: GoogleFonts.montserrat(
                                fontSize: 15, fontWeight: FontWeight.w500),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 14,
                ),
                LinearPercentIndicator(
                  width: 240,
                  animation: true,
                  lineHeight: 10.0,
                  percent: percent,
                  barRadius: const Radius.circular(15),
                  // ignore: deprecated_member_use
                  // linearStrokeCap: LinearStrokeCap.butt,
                  progressColor: const Color(0xffBCE954),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
