import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rebuild_5/sport_bar.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  bool isClicked = false;
  @override
  void setState(VoidCallback fn) {
    //isClicked=false;
    super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 237, 235, 235),
      body: SafeArea(
        child: Column(
          children: [
            Container(
              height: 215,
              width: 700,
              decoration: const BoxDecoration(
                  borderRadius:
                      BorderRadius.only(bottomRight: Radius.circular(60)),
                  color: Colors.white),
              child: Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, top: 25),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Hello,',
                              style: GoogleFonts.montserrat(
                                  fontSize: 20, fontWeight: FontWeight.w500),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Text(
                              'Emile Clark',
                              style: GoogleFonts.montserrat(
                                  fontSize: 25, fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                        Container(
                          height: 60,
                          width: 60,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              color: const Color.fromARGB(255, 200, 199, 199)),
                          child: Image.asset(
                            'assets/woman.png',
                            height: 40,
                          ),
                        )
                      ],
                    ),
                    const SizedBox(
                      height: 40,
                    ),
                    Row(
                      children: [
                        Column(
                          children: [
                            Text(
                              'Calories Today',
                              style: GoogleFonts.montserrat(
                                  fontSize: 20, fontWeight: FontWeight.w500),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Text(
                              '250',
                              style: GoogleFonts.montserrat(
                                  fontSize: 35, fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                        const SizedBox(
                          width: 40,
                        ),
                        Column(
                          children: [
                            Text(
                              'Your Goal',
                              style: GoogleFonts.montserrat(
                                  fontSize: 20,
                                  color:
                                      const Color.fromARGB(255, 190, 187, 187),
                                  fontWeight: FontWeight.w500),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Text(
                              '250',
                              style: GoogleFonts.montserrat(
                                  fontSize: 35,
                                  color:
                                      const Color.fromARGB(255, 190, 187, 187),
                                  fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 60, left: 20, right: 20),
              child: Row(
                children: [
                  Container(
                    height: 45,
                    width: 95,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: const Color(0xffDD571C)),
                    child: Center(
                      child: Text(
                        'Today',
                        style: GoogleFonts.montserrat(
                            fontSize: 18,
                            color: Colors.white,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 15,
                  ),
                  Container(
                    height: 45,
                    width: 95,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.white,
                    ),
                    child: Center(
                      child: Text(
                        'Week',
                        style: GoogleFonts.montserrat(
                            fontSize: 18, fontWeight: FontWeight.w500),
                      ),
                    ),
                  )
                ],
              ),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Padding(
              padding: const EdgeInsets.only(top: 30,),
              child: Column(
                children: [
                  SportBar(
                    imageUrl: 'assets/running.png',
                    sport: 'Running',
                    time: '1h',
                    percent: 0.57,
                    total: '57%',
                    ukuran: 100,
                  ),
                  SportBar(
                    imageUrl: 'assets/swimming.png',
                    sport: 'Swimming',
                    time: '2h',
                    percent: 0.57,
                    total: '57%',
                    ukuran: 90,
                  ),
                  SportBar(
                    imageUrl: 'assets/lotus.png',
                    sport: 'Yoga',
                    time: '35min',
                    percent: 0.57,
                    total: '57%',
                    ukuran: 100,
                  ),
                ],
              ),
            ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: SizedBox(
        height: 50,
        child: Row(
          children: <Widget>[
            InkWell(
              onTap: () {
                //setState(() {isClicked = true;});
              },
              child: SizedBox(
                width: 66,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "Home",
                      style: GoogleFonts.montserrat(
                        fontSize: 15,
                        fontWeight: FontWeight.w500,
                        color: isClicked !=false? const Color(0xffDD571C) : Colors.grey,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: Image.asset('assets/new-moon.png',
                      width: 5,
                      height: 5,
                      color: isClicked !=false?const Color(0xffDD571C) : const Color.fromARGB(255, 237, 235, 235),
                      ),
                    )
                  ],
                ),
              ),
            ),
            InkWell(
              onTap: () {
                //setState(() {isClicked = true;});
              },
              child: SizedBox(
                width: 66,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "Mail",
                      style: GoogleFonts.montserrat(
                        fontSize: 15,
                        fontWeight: FontWeight.w500,
                        color: isClicked !=true? const Color(0xffDD571C) : Colors.grey,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: Image.asset('assets/new-moon.png',
                      width: 5,
                      height: 5,
                      color: isClicked !=true?const Color(0xffDD571C) : const Color.fromARGB(255, 237, 235, 235),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Expanded(
              child: InkWell(
                onTap: () {
                  
                },
                child: Ink(
                  child: Container(
                    alignment: Alignment.center,
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.only(topLeft: Radius.circular(35)),
                      color: Color(0xffDD571C),
                    ),
                    child: Text(
                      "Start",
                      style: GoogleFonts.montserrat(
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                          color: Colors.white),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
